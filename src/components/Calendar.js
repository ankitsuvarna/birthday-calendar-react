import React from "react";

const Calendar = ({ weeks }) => {
  const weekdayEntries = (weekday) => {
    const birthdays = weeks[weekday];
    const total_birthdays = birthdays.length;
    
    if (birthdays && total_birthdays === 0) { 
      return (
        <div className="inner-box day--empty">
          <span>:S</span>
        </div>
      );
    } else {
      let classes = "";
      let colors_array = ["#374637", "#12bf12", "#ea91ae", "#18dfe8", "#e45f1f", "#0008ff", "#ce0aff", "#34a6e8", "#c3e82b", "#dcae45"];
      
      if (total_birthdays === 1)
      classes = "day--1";
      else if (total_birthdays >= 2 &&  total_birthdays <= 4)
      classes = "day--2";
      else if (total_birthdays >= 5 && total_birthdays <= 7)
      classes = "day--3";
      else if (total_birthdays >= 8)
      classes = "day--4";
      
      return birthdays.map(({ initials }, index) => {
        let random_no = Math.random() * colors_array.length;
        let colors = colors_array[Math.floor(random_no)];
        
        return (
          <div
            key={index}  
            className={`inner-box ${classes}`}
            style={{backgroundColor: colors}}
          >
            <span>{initials}</span>
          </div>
        )
      });
    }
  }

  const calendarWeekList = Object.keys(weeks).map((value, index) => (
    <div className="box" key={index}>
      <div className="header">{value.toUpperCase()}</div>
      <div className={`body ${value}`}>
        {weekdayEntries(value)}
      </div>
    </div>
  ));

  return (
    <div className="wrapper">
      {calendarWeekList}
    </div>
  );
}

export default Calendar;