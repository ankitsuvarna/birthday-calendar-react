import React from 'react';


const DataForm = ({ data, year, error, onUpdateData, errorHandler, onYearUpdate }) => {
  
  const handleTextAreaChange = (e) => {
    try {
      let data = JSON.parse(e.target.value);
      onUpdateData(data);
      errorHandler();
    } catch (er) {
      errorHandler("There are some syntax error in JSON text. Please correct it");
    }
  }
  
  const handleYearUpdate = (e) => {
    let year = e.target.value;
    
    onYearUpdate(year);
    errorHandler();
  }
  
  const handleDataUpdate = (e) => {
    e.preventDefault();
  
    if (parseInt(year, 10) < 1970) 
      errorHandler("Year must be greater greater than 1970");
    else
      onUpdateData(data, year)
  }

  return (
    <div className="form-content">
      <textarea
        name="datasource"
        defaultValue={JSON.stringify(data, null, 4)}
        onChange={handleTextAreaChange.bind(this)}
        rows="30" cols="50" />
      <div className="yearField">
        <label htmlFor="year-input">Year</label>
        <input
          type="number"
          id="year-input"
          name="year"
          value={year}
          onChange={handleYearUpdate.bind(this)}
        />
        {
          error && <div className="error-messages">{error}</div>
        }
        <button
          className="big-btn"
          onClick={handleDataUpdate.bind(this)}
        >Update</button>
      </div>
    </div>
  );
}

export default DataForm;