import React, { Component } from 'react';
import GOT from './data/birthday-text';

import Calendar from './components/Calendar';
import DataForm from './components/DataForm';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      birth_data: [],
      weeks: {
        sun: [],
        mon: [],
        tue: [],
        wed: [],
        thu: [],
        fri: [],
        sat: [],
      },
      year: '2017',
      error: ''
    }
  }

  componentWillMount() {
    this.setState({ birth_data: GOT }, this.processData.bind(this));
    
  }

  async processData() {
    let person = this.state.birth_data;
    let weeks = {
      sun: [],
      mon: [],
      tue: [],
      wed: [],
      thu: [],
      fri: [],
      sat: [],
    };
    let error_counter = 0, error_names = "";

    for (let i = 0, j = person.length; i < j; i++) {
      if (person[i].name && person[i].birthday) {
        
        let initials = this.parseNameInitials(person[i].name);
        let birth_data = this.getWeekDayAndAge(person[i].birthday, this.state.year);
  
        if (!birth_data) {
          error_counter++;
          error_names += `, ${person[i].name}`;
        } else {
          weeks[birth_data.weekday].push({
            initials: initials,
            age: birth_data.age
          });
        }

      }

    }

    if (error_counter === 0) {
      await this.setState({ 'error': '' });
    } else {
      error_names = `Invalid date format for${error_names}. Please use 'mm/dd/yyyy' format.`
      await this.setState({ 'error': error_names });
    } 


    //Ordering people inside the card based on their age(young to old)
    for (let prop in weeks) {
      weeks[prop].sort(function (a, b) {
        return a.age - b.age;
      });
    }

    await this.setState({ weeks });
  }

  parseNameInitials(names) {
    let initials = names.match(/\b([A-Z])/g);
    return initials.join('');
  }

  getWeekDayAndAge(date, year) {
    // date format: mm/dd/yyyy
    let getMonth = date.split('/')[0];
    let getDay = date.split('/')[1];
    let birth_year = date.split('/')[2];
    
    let age = year - birth_year;
    let weekday = new Date(getMonth + '/' + getDay + '/' + year).getDay();
    
    switch (weekday) {
      case 0:
        return { weekday: 'sun', age };
      case 1:
        return { weekday: 'mon', age };
      case 2:
        return { weekday: 'tue', age };
      case 3:
        return { weekday: 'wed', age };
      case 4:
        return { weekday: 'thu', age };
      case 5:
        return { weekday: 'fri', age };
      case 6:
        return { weekday: 'sat', age };
      default:
      return null;
    }
  }

  handleUpdateData(birth_data = this.state.birth_data, year = this.state.year) {
    this.setState({ birth_data, year }, this.processData.bind(this));
  }

  handleYearUpdate(year) {
    this.setState({ year });
  }

  handleErrorHandler(error = "") {
    this.setState({ error });
  }

  render() {
    const { birth_data, year, weeks, error } = this.state;

    return (
      <div>
        <h1 className="main-header">Birthday Cal</h1>  
        <Calendar weeks={weeks}/>
        <DataForm
          data={birth_data}
          onUpdateData={this.handleUpdateData.bind(this)}
          onYearUpdate={this.handleYearUpdate.bind(this)}
          year={year}
          error={error}
          errorHandler={this.handleErrorHandler.bind(this)}
        />
      </div>      
    );
  }
}

export default App;
